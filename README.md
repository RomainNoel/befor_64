# BeFor_64

A single file libary for encoding using a base 64 in Fortran.
This lib is largely adapted from (BeFoR64)[https://github.com/szaghi/BeFoR64]


## Requirements
* (PorPre)(https://gitlab.com/RomainNoel/porpre) a portable precision library
